## Customized version of https://gitlab.com/kivymd/KivyMD
================================================================

### Need to see how it works?

Just install **Kivy 1.9.2-dev0** and execute `demo.py`

### Demo

https://drive.google.com/file/d/0B0ZLJ2Diih0oeU41V0JKZWF6Q0k/view?usp=sharing
